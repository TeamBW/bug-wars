﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class AttackProperties : AttackMetaData {

	public string Attack_ID;
	
	public AttackType attackType;

	public int noOfAttacks;

	public int baseDamage;
}
