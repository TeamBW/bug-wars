﻿using UnityEngine;
using System.Collections;

public class UIManager : MonoBehaviour {

	// Load a Level
	public void NavigateTo(int scene)
	{
		Application.LoadLevel(scene);
	}
	// Load a Level
	public void ExitGame()
	{
		Application.Quit();
	}

}
